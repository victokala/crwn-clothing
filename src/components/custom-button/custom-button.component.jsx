import React from 'react';
import './custom-button.styles.scss';

const CustomButtom = ({children, className, ...otherProps}) => (
    <button className={`custom-button ${className ? className : ''}`} {...otherProps}>
        {children}
    </button>
);

export default CustomButtom;