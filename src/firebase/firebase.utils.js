import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const config = {
    apiKey: "AIzaSyBGCSSantiqTAJ-x10Hq7bBylyip1EVFZg",
    authDomain: "crwn-db-ebbc1.firebaseapp.com",
    databaseURL: "https://crwn-db-ebbc1.firebaseio.com",
    projectId: "crwn-db-ebbc1",
    storageBucket: "",
    messagingSenderId: "280345966474",
    appId: "1:280345966474:web:4a1c27b1900234ef"
};

firebase.initializeApp(config);

export const firestore = firebase.firestore();

export const createUserProfileDocument = async (userAuth, additionalData) => {
    if(!userAuth){
        return;
    }
    const userRef = firestore.doc(`users/${userAuth.uid}`);
    const snapShot = await userRef.get();
    if(!snapShot.exists){
        const {displayName, email, photoURL} = userAuth;
        const createdAt = new Date();

        try{
            await userRef.set({
                displayName,
                email,
                photoURL,
                createdAt,
                ...additionalData
            });
        }catch(error){
            console.error('error creating user', error.message);
        }
    }
    return userRef;
};


export const auth = firebase.auth();

const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({prompt: 'select_account'});
export const signInWithGoogle = () => auth.signInWithPopup(provider);

export default firebase;